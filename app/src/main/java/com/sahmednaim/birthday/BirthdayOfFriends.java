package com.sahmednaim.birthday;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class BirthdayOfFriends extends AppCompatActivity
{
    TextView textView;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_birthday_of_friends);

        textView = (TextView) findViewById(R.id.show_birthday);
        listView = (ListView) findViewById(R.id.list_birthday);

        MyDBFunctions mf = new MyDBFunctions(getApplicationContext());

        String[] data = mf.my_data();

//        String s = "";
//
//        for (int i = 0; i < data.length; i++)
//        {
//            s = s + data[i] + "\n\n";
//        }

//        textView.setText(s);

        // ListView Implementation
        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.layout_birthday, R.id.name_and_date, data);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                Intent intent = new Intent(getApplicationContext(), SingleBirthday.class);
                intent.putExtra("MyKey", i);
                startActivity(intent);
            }
        });

    }
}
