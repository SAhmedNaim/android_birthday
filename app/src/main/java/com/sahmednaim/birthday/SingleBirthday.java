package com.sahmednaim.birthday;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SingleBirthday extends AppCompatActivity
{

    EditText editText;
    Button update, delete;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_birthday);

        editText = (EditText) findViewById(R.id.edit_text);
        update = (Button) findViewById(R.id.update);
        delete = (Button) findViewById(R.id.delete);

        final int receive_position = getIntent().getIntExtra("MyKey", 999);

        final MyDBFunctions myDBFunctions = new MyDBFunctions(getApplicationContext());

        String text = myDBFunctions.fetch_day(receive_position + 1);

        editText.setText(text);

        editText.setSelection(editText.getText().length());

        update.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                myDBFunctions.update_birthday((receive_position + 1), editText.getText().toString());
                Toast.makeText(SingleBirthday.this, "Updated Successfully!!!", Toast.LENGTH_SHORT).show();
            }
        });

        delete.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                myDBFunctions.delete_birthday(myDBFunctions.fetch_day(receive_position + 1));
                Toast.makeText(SingleBirthday.this, "Data Deleted Successfully!!", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
