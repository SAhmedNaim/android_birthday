package com.sahmednaim.birthday;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{

    EditText name, birthday;
    Button save, show;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = (EditText) findViewById(R.id.name);
        birthday = (EditText) findViewById(R.id.birthday);

        save = (Button) findViewById(R.id.save);
        show = (Button) findViewById(R.id.show);

        final MyDBFunctions mf = new MyDBFunctions(getApplicationContext());

        save.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                String _name = name.getText().toString();
                String _birthday = birthday.getText().toString();

                DataTemp dt = new DataTemp(_name, _birthday);

                mf.addDataToTable(dt);

                Toast.makeText(getApplicationContext(), "Data Added Successfully!!!", Toast.LENGTH_SHORT).show();
            }
        });

        show.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(getApplicationContext(), BirthdayOfFriends.class);
                startActivity(intent);
            }
        });

    }
}
